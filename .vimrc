behave mswin

vnoremap <Left> h
vnoremap <Right> l
vnoremap <Up> k
vnoremap <Down> j

set diffexpr=MyDiff()
function! MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  if &sh =~ '\<cmd'
    silent execute '!""C:\Program Files\Vim\vim64\diff" ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . '"'
  else
    silent execute '!C:\Program" Files\Vim\vim64\diff" ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3
  endif
endfunction

set nocompatible 	" don't need to be compatible with vi.
set modelines=0  	" avoid security issues with modelines in files
set number			" show line numbers
set nowrap			" don't wrap text, get a bigger monitor.
set nobackup
set nowritebackup
set wrapmargin=0
set noautoindent

set shiftwidth=4	" tab = 4 spaces
set tabstop=4		" tab = 4 spaces
set softtabstop=4	" tab = 4 spaces
"set expandtab		" convert tab to spaces.

" Other stuff...
set encoding=utf-8
set scrolloff=3
set showmode
set showcmd
set wildmenu					" tab completion menu at the bottom
set wildmode=list:longest		" tab completion menu at the bottom
set visualbell
set mouse=a						" mouse support in the console
set cursorline
set ttyfast
set backspace=indent,eol,start	" make backspace work normally.
"set laststatus=2
set relativenumber
"set undofile


set hlsearch
set incsearch
set showmatch
set ignorecase
set smartcase
syntax on
set showmatch
set ruler
colorscheme darkblue
set background=dark

set guifont=Bitstream\ Vera\ Sans\ Mono:h14

autocmd BufEnter * lcd %:p:h  " change to directory of current file automatically.
autocmd bufwritepost .vimrc source $MYVIMRC

au BufNewFile,BufRead *.as set syntax=actionscript
au BufNewFile,BufRead *.fx set syntax=fx
au BufNewFile,BufRead *.fxh set syntax=fx
au BufNewFile,BufRead *.build set syntax=xml
au BufNewFile,BufRead *.rvi set syntax=xml
au BufNewFile,BufRead *.lnx set syntax=xml

" minibufexplorer stuff
let g:miniBufExplVSplit = 20   " column width in chars
let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1

" for omnicppcomplete
set nocp
filetype plugin on
set tags+=C:\MyPersonalData\FNB\vimTags\tags


" Key mappings...

" open for edit with p4
map ,o :!p4 edit %:p<CR>

" copy current file to xbox
"map ,x :w<CR> :!xbcp /y /f /x xbdeuro07 %:p xe:\fifaxbox\ <CR>
map ,x :w<CR> :!"C:\Program Files\Microsoft Xbox 360 SDK\bin\win32\xbcp" /y /f /x fnb_ravi %:p xe:\fnb\data\temp\ <CR>

" for the shaders... need to learn vim script :P
map ,s :w<CR> :!"C:\Program Files\Microsoft Xbox 360 SDK\bin\win32\xbcp" /y /f /x fnb_ravi %:p xe:\fnb\data\xenon\shaders\ <CR>

" toggle line numbers
map <F12> :set number!<CR>



